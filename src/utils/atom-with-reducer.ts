import { atom, WritableAtom } from 'jotai'

/**
 * atom を reducer で定義する
 *
 * reducer 内で dispatch を呼べる
 */
export function atomWithReducer<S, M>(
	initialValue: S,
	reducer: (s: S, m: M) => S,
): WritableAtom<S, M, void> {
	type T = { t: M }
	const data = atom<S>(initialValue)
	const stack = atom<[ready: boolean, list: T[]]>([true, []])
	return atom(
		get => get(data),
		(get, set, t: M) => {
			let [r, l] = get(stack)
			if (r) {
				try {
					for (let p: T | undefined = { t }; p; [p, ...l] = get(stack)[1]) {
						set(stack, [!r, l])
						set(data, reducer(get(data), p.t))
					}
				} finally {
					set(stack, [r, l])
				}
				return
			}
			set(stack, [r, [...l, { t }]])
		},
	)
}
