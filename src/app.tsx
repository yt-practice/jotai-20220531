import { Count, Reset, Up } from './comps/count'
import { UseFn } from './comps/use-fn'

export const App = () => (
	<div>
		<h1>app</h1>
		<hr />
		<Count />
		<hr />
		<Up />
		<Reset />
		<hr />
		<UseFn />
	</div>
)
