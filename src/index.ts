import { main } from './main'

Promise.resolve()
	.then(main)
	.catch(x => {
		console.error(x)
	})
