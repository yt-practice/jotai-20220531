/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/naming-convention */

import { unreachable } from '../utils/unreachable'

import type { State } from './state'

const count = {
	'count:inc': (st: State): State => ({ ...st, count: st.count + 1 }),
	'count:dec': (st: State): State => ({ ...st, count: st.count - 1 }),
	'count:reset': (st: State): State => ({ ...st, count: 0 }),
} as const

const useFn = {
	'use:fn': (st: State, mut: { fn: (v: string) => string }): State => ({
		...st,
		val: mut.fn(st.val),
	}),
} as const

const map = {
	...count,
	...useFn,
} as const

type MyFn<S> = (s: S, m: any) => S
type GetSecPrm<F extends (...p: any[]) => any> = F extends (
	s: any,
	m: infer M,
) => any
	? M
	: never

export type GetMuts<S, T extends Record<string, MyFn<S>>> = {
	[K in keyof T]: Readonly<{ type: K } & GetSecPrm<T[K]>>
}[keyof T]

export type Mutation = GetMuts<State, typeof map>

export const reducer = (st: State, mut: Mutation): State => {
	console.log({ st, mut })
	if (Object.prototype.hasOwnProperty.call(map, mut.type))
		// @ts-expect-error: ignore
		return map[mut.type](st, mut)
	return unreachable(mut as never)
}
