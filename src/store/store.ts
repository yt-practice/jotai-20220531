import { Atom, atom, useAtom } from 'jotai'
import { useUpdateAtom } from 'jotai/utils'
import { useRef } from 'preact/hooks'

import { atomWithReducer } from '../utils/atom-with-reducer'

import { reducer } from './reducer'
import { initedState, State } from './state'

const storeAtom = atomWithReducer(initedState, reducer)

export const useApp = <T>(select: (st: State) => T) => {
	const rf = useRef<Atom<T> | null>(null)
	if (null === rf.current) rf.current = atom(get => select(get(storeAtom)))
	return useAtom(rf.current)[0]
}
export const useAppDispatch = () => useUpdateAtom(storeAtom)
