export type State = Readonly<{
	count: number
	val: string
}>

export const initedState: State = {
	count: 0,
	val: '',
}
