import { render } from 'preact'

import { App } from './app'

export const main = () => {
	const main = document.createElement('main')
	document.body.appendChild(main)
	render(<App />, main)
}
