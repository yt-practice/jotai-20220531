import { useApp, useAppDispatch } from '~/store/store'

export const Count = () => {
	const cnt = useApp(s => s.count)
	return <div>count: {cnt}</div>
}

export const Up = () => {
	const dispatch = useAppDispatch()
	return (
		<button
			onClick={() => {
				dispatch({ type: 'count:inc' })
			}}
		>
			+1
		</button>
	)
}

export const Reset = () => {
	const dispatch = useAppDispatch()
	return (
		<button
			onClick={() => {
				dispatch({ type: 'count:reset' })
			}}
		>
			reset
		</button>
	)
}
