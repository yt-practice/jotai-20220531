import { useApp, useAppDispatch } from '~/store/store'

export const UseFn = () => {
	const dispatch = useAppDispatch()
	const val = useApp(s => s.val)
	return (
		<div>
			<button
				onClick={() => {
					console.log('click')
					dispatch({
						type: 'use:fn',
						fn: v => {
							dispatch({ type: 'count:inc' })
							return v + 'h'
						},
					})
				}}
			>
				h
			</button>
			<output>{val}</output>
		</div>
	)
}
